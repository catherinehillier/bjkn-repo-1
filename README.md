
# Assessment 3 BJKN README   :books: #

## AWS Cloudformation   :cloud: ##

### Create VPC ###

1. Log into the Jenkins server with the following link: [Jenkins](http://ec2-3-8-96-102.eu-west-2.compute.amazonaws.com:8080/)

        * User: admin
        * Password: bobbysjenkins


2. To run the script to create the VPC and subnets:

    * Click on the job named *Katie-createvpc.xml*.
    * Click *Build with Parameters* and you can change any values if you wish, otherwise it will build with the default values given. The name of the stack that will be created is **bjknstack**. If you would like to change this name, before clicking build, go to *Configure* and scroll to the bottom. You can change the stack name on the *Execute shell command*, under *parameters*.
    * Finally, click *Build* and a stack will create to build the VPC.


3. To check that everything is a-okay, you can view the stack progress on the aws website: [AWS stacks](https://eu-west-2.console.aws.amazon.com/cloudformation/home?region=eu-west-2#/stacks?filter=active) (You may need to enter your log in details). Click on your stack name to see the progress of the stack.

4. Once the stack has completed, you can view the VPC and all its components on the aws website: [AWS VPC](https://eu-west-2.console.aws.amazon.com/vpc/home?region=eu-west-2#vpcs:sort=VpcId)

5. If for some reason, you have created this stack by accident and you wish to delete the VPC, you can run the job on the Jenkins server called *Katie-destroyvpc.xml* and it will destroy the stack.

### RDS ###

1. Log into the Jenkins server with the following link: [Jenkins](http://ec2-3-8-96-102.eu-west-2.compute.amazonaws.com:8080/)

        * User: admin
        * Password: bobbysjenkins

2. To run the script to create the VPC and subnets:

    * Click on the job named *jons-build-RDS.xml*.
    * Finally, click *Build* and an RDS will be built.
    * After you replace the ami in the ASG.yml, copy the file to the bjkn-test-bucket. Then run the job called create ASG and this creates the auto scaling group, elastic load balancers and the petclinic instances.

### petclinic :dog: :cat: ####

1. Log into the Jenkins server with the following link: [Jenkins](http://ec2-3-8-96-102.eu-west-2.compute.amazonaws.com:8080/)

        * User: admin
        * Password: bobbysjenkins

2. To run the script to compile the petclinic java file:
   * Click on the job named *bobbys-petcliniccompiler.xml*.

  * Finally, click *Build with parameters* and paste the cluster end point into the DBSVS parameter.This will compile the petclinic app and send the java file to the bjkn-bucket s3 bucket.
3. Next you will need to go to your chosen terminal and cd into the bjkn-repo-1 directory.
4. Once there you will have to run the command `ansible-playbook -i environment/prod site.yml`. this will create an AMI of the petclinic app that will be used to in the next cloudformation section.
